Rails.application.routes.draw do
  root "rooms#index"
  get '/sign', to: 'sessions#new'
  post '/sign', to: 'sessions#create'
  delete '/sign', to: 'sessions#destroy'
  get '/signup', to: 'sign#new'
  post '/signup', to: 'sign#create'
  get '/rooms/:id', to: 'rooms#show'
  get '/rooms/:id/code', to: 'rooms#code'
  post '/rooms/:id/code', to: 'rooms#get_code'
  post '/rooms/:id/code', to: 'rooms#code'
  post '/rooms/:id', to: 'messages#create'


  resources :rooms do
    resources :messages
  end
  resources :users
end
