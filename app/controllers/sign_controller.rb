class SignController < ApplicationController
  def index
    @user=User.new
  end
  def sign
    @user=User.new
  end
  def signup
    @user=User.new
  end
  def new
    @user = User.new
  end
  def create
    if params[:sign][:password] == params[:sign][:password_confirmation] and params[:sign][:username] != '' and params[:sign][:password] != ''
      user = User.create(username: params[:sign][:username],password: params[:sign][:password])
      log_in(user)
    end
  end
end
