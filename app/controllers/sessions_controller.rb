class SessionsController < ApplicationController

  def create
    user = User.find_by(username: params[:session][:username], password: params[:session][:password])
    if user
      log_in(user)
    else
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_path
  end
  def new_user
    @user = User.add(username: params[:sign][:username],password: params[:sign][:password])
  end
end
