class MessagesController < ApplicationController
  def create
    @id=params['id']
    @c_user = current_user
    @c_user = @c_user.id
    if params[:new_message][:body] != ''
      Message.create(user_id: @c_user, body: params[:new_message][:body], room_id: @id)
      redirect_to '/rooms/' + @id
    end
  end

  private

  def msg_params
    params.require(:message).permit(:content)
  end
end
