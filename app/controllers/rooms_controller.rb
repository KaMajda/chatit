class RoomsController < ApplicationController
  def index
    @message = Message.new
    @room = Room.new
    @current_user = current_user
    redirect_to '/sign' unless @current_user
    @rooms = Room.all
    @users = User.all
  end
  def code
    @room = Room.find(params[:id])
  end
  def create
    @message = Message.new
    if params["room"]["name"] and params['room']['code'] != ''
      @room = Room.create(name: params["room"]["name"], code: params["room"]["code"])
      session[:roomcode] = params['room']['code']
      redirect_to @room
    end
  end
  def show
    @user = current_user
    @send = ( 'app/assets/images/send.svg' )
    @room = Room.find(params[:id])
    #@messages = @room.messages
    @id=params['id']
    @message = Message.new
    @messages = Message.all
    @code = ''
    if session[:roomcode] != nil
      @code = session[:roomcode]
      @code = @code["roomcode"]
    end
    red = '/rooms/' + @id + '/' + 'code'
    if @code != @room.code
      redirect_to red
    end
  end
  def code
    @code = params['roomcode']
    @id=params['id']
  end
  def get_code
    @id=params['id']
    @c_room = Room.find(@id)
    code=params['roomcode'].to_enum.to_h
    red = '/rooms/' + @id
    session[:roomcode] = code
    @c=session[:roomcode]
    @c=@c['roomcode']
    if @c == @c_room.code
      redirect_to red
    end
  end
end
